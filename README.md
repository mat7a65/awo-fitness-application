# awo-project
This was group-project during the academy-training.
In this project we developed a fullstack fitness-application in which you can create and view workout-plans.

# Tech-Stack, Frameworks and Utilities:

## Backend
- Java
- Spring-Framework
- Spring-Security
- Hibernate
- MariaDB, HeidiSQL
- Maven
- Spring Boot

## Frontend
- Angular 9
- TypeScript
- HTML5
- CSS3
- Bootstrap

## Developed using:
- IntelliJ Idea
- Git, Gitlab
- Trello