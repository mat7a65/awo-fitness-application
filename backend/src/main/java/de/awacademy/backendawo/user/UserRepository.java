package de.awacademy.backendawo.user;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface UserRepository extends CrudRepository<User, Integer> {
    Optional<User> findByUserNameIgnoreCase(String username);
    Optional<User> findByUserName(String username);
    Optional<User> findByUserId(int userId);

    boolean existsByUserNameIgnoreCase(String username);
}
