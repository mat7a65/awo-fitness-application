package de.awacademy.backendawo.user;

import de.awacademy.backendawo.userexercise.UserExercise;
import de.awacademy.backendawo.workoutPlan.WorkoutPlan;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int userId;

    private String userName;

    private String userPassword;

    @OneToMany(mappedBy = "user")
    private List<WorkoutPlan> workoutPlanList;

    @OneToMany(mappedBy = "user")
    private List<UserExercise> userExerciseList;

    public User(String userName, String userPassword) {
        this.userName = userName;
        this.userPassword = userPassword;
    }
    public User() {
    }

    public List<UserExercise> getUserExerciseList() {
        return userExerciseList;
    }

    public void setUserExerciseList(List<UserExercise> userExerciseList) {
        this.userExerciseList = userExerciseList;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getUserPassword() {
        return userPassword;
    }

    public void setUserPassword(String userPassword) {
        this.userPassword = userPassword;
    }

    public List<WorkoutPlan> getWorkoutPlanList() {
        return workoutPlanList;
    }

    public void setWorkoutPlanList(List<WorkoutPlan> workoutPlanList) {
        this.workoutPlanList = workoutPlanList;
    }

    public void addWorkoutPlan(WorkoutPlan workoutPlan) {
        this.workoutPlanList.add(workoutPlan);
    }

}
