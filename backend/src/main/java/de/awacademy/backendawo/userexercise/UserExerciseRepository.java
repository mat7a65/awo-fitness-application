package de.awacademy.backendawo.userexercise;

import de.awacademy.backendawo.exercise.Exercise;
import de.awacademy.backendawo.user.User;
import de.awacademy.backendawo.workoutPlan.WorkoutPlan;
import org.springframework.data.repository.CrudRepository;

import java.util.List;
import java.util.Optional;

public interface UserExerciseRepository extends CrudRepository<UserExercise, Integer> {
    List<UserExercise> findAll();
    List<UserExercise> findAllByUserUserId(Integer id);
    List<UserExercise> findAllByUserUserName(String userName);
    List<UserExercise> findAllByExerciseExerciseId(Integer id);
    List<UserExercise> findAllByExerciseTitle(String exerciseTitle);
    Optional<UserExercise> findById(int id);
    Optional<UserExercise> findByExercise_Title(String title);
    Optional<UserExercise> findByUser_UserId(Integer userId);
    Optional<UserExercise> findByUser_UserIdAndExercise_ExerciseId(Integer userId, Integer exerciseId);
    Optional<UserExercise> findByUser_UserNameAndExercise_Title(String userName, String exerciseTitle);


}
