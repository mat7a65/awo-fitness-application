package de.awacademy.backendawo.userexercise;

import de.awacademy.backendawo.workoutPlan.WorkoutPlan;
import de.awacademy.backendawo.workoutPlan.WorkoutPlanDTO;
import de.awacademy.backendawo.workoutPlan.WorkoutPlanRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

@Service
public class UserExerciseService {

    @Autowired
    private UserExerciseRepository userExerciseRepository;

    public List<UserExerciseDTO> userExerciseServiceList (){
        List<UserExerciseDTO> response = new ArrayList<>();

        for (UserExercise userExercise : userExerciseRepository.findAll()) {
            response.add(new UserExerciseDTO(userExercise.getSets(), userExercise.getRepetitions(), userExercise.getWeight(), userExercise.getDauer(), userExercise.getPuls(), userExercise.getImpression()));
        }
        return response;
    }

    public UserExerciseDTO userExercise(String userName, String exerciseTitel) {

        UserExercise userExercise = userExerciseRepository.findByUser_UserNameAndExercise_Title(userName, exerciseTitel).get();
        return new UserExerciseDTO(userExercise.getSets(), userExercise.getRepetitions(), userExercise.getWeight(), userExercise.getDauer(), userExercise.getPuls(), userExercise.getImpression());

    }

    public UserExerciseDTO saveUserExercise(String userName, String exerciseTitel, UserExerciseDTO userExerciseDTO){
        UserExercise userExercise = userExerciseRepository.findByUser_UserNameAndExercise_Title(userName, exerciseTitel).get();
        userExercise.setSets(userExerciseDTO.getSets());
        userExercise.setRepetitions(userExerciseDTO.getRepetitions());
        userExercise.setWeight(userExerciseDTO.getWeight());
        userExercise.setDauer(userExerciseDTO.getDauer());
        userExercise.setPuls(userExerciseDTO.getPuls());
        userExercise.setImpression(userExerciseDTO.getImpression());
        userExerciseRepository.save(userExercise);
        return userExerciseDTO;
    }
}
