package de.awacademy.backendawo.userexercise;


import de.awacademy.backendawo.workoutPlan.WorkoutPlanDTO;
import de.awacademy.backendawo.workoutPlan.WorkoutPlanService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class UserExerciseController {

    @Autowired
    private UserExerciseService userExerciseService;

    public UserExerciseController(UserExerciseService userExerciseService) {
        this.userExerciseService = userExerciseService;
    }

    @GetMapping("/api/userExercise/")
    public List<UserExerciseDTO> view() {
        return userExerciseService.userExerciseServiceList();
    }

    @GetMapping("/api/userExercise/{userName}/{exerciseTitel}")
    public UserExerciseDTO view(@PathVariable String userName, @PathVariable String exerciseTitel) {
        return userExerciseService.userExercise(userName, exerciseTitel);
    }

    @PostMapping("/api/userExercise/{userName}/{exerciseTitel}")
    public UserExerciseDTO save(@PathVariable String userName, @PathVariable String exerciseTitel, @RequestBody UserExerciseDTO userExerciseDTO) {
        return userExerciseService.saveUserExercise(userName, exerciseTitel, userExerciseDTO);
    }
}
