package de.awacademy.backendawo.userexercise;

public class UserExerciseDTO {

    private int sets;

    private int repetitions;

    private int weight;

    private int dauer;

    private int puls;

    private String impression;

    public UserExerciseDTO(int sets, int repetitions, int weight, int dauer, int puls, String impression) {
        this.sets = sets;
        this.repetitions = repetitions;
        this.weight = weight;
        this.dauer = dauer;
        this.puls = puls;
        this.impression = impression;
    }

    public int getSets() {
        return sets;
    }

    public void setSets(int sets) {
        this.sets = sets;
    }

    public int getRepetitions() {
        return repetitions;
    }

    public void setRepetitions(int repetitions) {
        this.repetitions = repetitions;
    }

    public int getWeight() {
        return weight;
    }

    public void setWeight(int weight) {
        this.weight = weight;
    }

    public int getDauer() {
        return dauer;
    }

    public void setDauer(int dauer) {
        this.dauer = dauer;
    }

    public int getPuls() {
        return puls;
    }

    public void setPuls(int puls) {
        this.puls = puls;
    }

    public String getImpression() {
        return impression;
    }

    public void setImpression(String impression) {
        this.impression = impression;
    }
}
