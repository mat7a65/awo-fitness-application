package de.awacademy.backendawo.userexercise;

import de.awacademy.backendawo.exercise.Exercise;
import de.awacademy.backendawo.user.User;

import javax.persistence.*;
import java.util.List;

@Entity
public class UserExercise {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    private int sets;

    private int repetitions;

    private int weight;

    private int dauer;

    private int puls;

    private String impression;

    @ManyToOne
    private User user;

    @ManyToOne
    private Exercise exercise;

    public UserExercise(){}

    public UserExercise(User user, Exercise exercise){
        this.user = user;
        this.exercise = exercise;
        this.impression = "";
    }

    public UserExercise(User user, Exercise exercise, int sets, int repetitions) {
        this.sets = sets;
        this.repetitions = repetitions;
        this.user = user;
        this.exercise = exercise;
        this.impression = "";
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getSets() {
        return sets;
    }

    public void setSets(int sets) {
        this.sets = sets;
    }

    public int getRepetitions() {
        return repetitions;
    }

    public void setRepetitions(int repetitions) {
        this.repetitions = repetitions;
    }

    public int getWeight() {
        return weight;
    }

    public void setWeight(int weight) {
        this.weight = weight;
    }

    public int getDauer() {
        return dauer;
    }

    public void setDauer(int dauer) {
        this.dauer = dauer;
    }

    public int getPuls() {
        return puls;
    }

    public void setPuls(int puls) {
        this.puls = puls;
    }

    public String getImpression() {
        return impression;
    }

    public void setImpression(String impression) {
        this.impression = impression;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Exercise getExercise() {
        return exercise;
    }

    public void setExercise(Exercise exercise) {
        this.exercise = exercise;
    }
}
