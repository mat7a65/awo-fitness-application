package de.awacademy.backendawo.exercise;

import de.awacademy.backendawo.userexercise.UserExercise;
import de.awacademy.backendawo.workoutSession.WorkoutSession;

import javax.persistence.*;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Entity
public class Exercise {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int exerciseId;

    private String title;

    @Column(columnDefinition = "TEXT")
    private String description;

    private int sets;

    private int repetitions;

    private int weight;

    private int dauer;

    private int puls;

    private String category;

    private String muscleType;



    public Exercise() {
    }



    public Exercise(String title, String description, int sets, int repetitions, int weight, String category, String muscleType) {
        this.title = title;
        this.description = description;
        this.sets = sets;
        this.repetitions = repetitions;
        this.weight = weight;
        this.category = category;
        this.muscleType = muscleType;
    }

    public Exercise(String title, String description, int dauer, int puls, String category, String muscleType) {
        this.title = title;
        this.description = description;
        this.dauer = dauer;
        this.puls = puls;
        this.category = category;
        this.muscleType = muscleType;
    }


    public int getDauer() {
        return dauer;
    }

    public void setDauer(int dauer) {
        this.dauer = dauer;
    }

    public int getPuls() {
        return puls;
    }

    public void setPuls(int puls) {
        this.puls = puls;
    }


    public int getExerciseId() {
        return exerciseId;
    }

    public void setExerciseId(int exerciseId) {
        this.exerciseId = exerciseId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getSets() {
        return sets;
    }

    public void setSets(int sets) {
        this.sets = sets;
    }

    public int getRepetitions() {
        return repetitions;
    }

    public void setRepetitions(int repetitions) {
        this.repetitions = repetitions;
    }

    public int getWeight() {
        return weight;
    }

    public void setWeight(int weight) {
        this.weight = weight;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getMuscleType() {
        return muscleType;
    }

    public void setMuscleType(String muscleType) {
        this.muscleType = muscleType;
    }

//    public WorkoutSession getWorkoutSession() {
//        return workoutSession;
//    }
//
//    public void setWorkoutSession(WorkoutSession workoutSession) {
//        this.workoutSession = workoutSession;
//    }
}
