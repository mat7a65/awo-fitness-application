package de.awacademy.backendawo.exercise;

import de.awacademy.backendawo.workoutPlan.WorkoutPlanDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.util.LinkedList;
import java.util.List;

@RestController
public class ExerciseController {

    @Autowired
    private ExerciseService exerciseService;

    public ExerciseController(ExerciseService exerciseService) {
        this.exerciseService = exerciseService;
    }

    @GetMapping("/api/exercises")
    public List<ExerciseDTO> view() {
        return exerciseService.exerciseList();
    }

    @GetMapping("/api/exercises/{exerciseTitle}")
    public ExerciseDTO viewExersice(@PathVariable String exerciseTitle) {
        return exerciseService.getExercise(exerciseTitle);
    }

    @GetMapping("/api/exercisecategory/{category}")
    public List<ExerciseDTO> viewDetails(@PathVariable String category) {
        return exerciseService.exerciseListCategory(category);
    }

}
