package de.awacademy.backendawo.exercise;

public class ExerciseDTO {

    private String title;

    private String description;

    private int sets;

    private int repetitions;

    private int weight;

    private String category;

    private String muscleType;

    public ExerciseDTO () {

    }

    public ExerciseDTO(String title, String description, int sets, int repetitions, int weight, String category, String muscleType) {
        this.title = title;
        this.description = description;
        this.sets = sets;
        this.repetitions = repetitions;
        this.weight = weight;
        this.category = category;
        this.muscleType = muscleType;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getSets() {
        return sets;
    }

    public void setSets(int sets) {
        this.sets = sets;
    }

    public int getRepetitions() {
        return repetitions;
    }

    public void setRepetitions(int repetitions) {
        this.repetitions = repetitions;
    }

    public int getWeight() {
        return weight;
    }

    public void setWeight(int weight) {
        this.weight = weight;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getMuscleType() {
        return muscleType;
    }

    public void setMuscleType(String muscleType) {
        this.muscleType = muscleType;
    }
}
