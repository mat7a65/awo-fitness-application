package de.awacademy.backendawo.exercise;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.LinkedList;
import java.util.List;

@Service
public class ExerciseService {

    @Autowired
    private ExerciseRepository exerciseRepository;

    public List<ExerciseDTO> exerciseList (){
        List<ExerciseDTO> response = new LinkedList<>();

        for (Exercise exercise : exerciseRepository.findAll()) {
            response.add(new ExerciseDTO(exercise.getTitle(),exercise.getDescription(),exercise.getSets(), exercise.getRepetitions(),
                    exercise.getWeight(), exercise.getCategory(), exercise.getMuscleType()));
        }
        return response;
    }

    public List<ExerciseDTO> exerciseListCategory (String category){
        List<ExerciseDTO> responseCat = new LinkedList<>();

        for (Exercise exercise : exerciseRepository.findAllByCategory(category)) {
            responseCat.add(new ExerciseDTO(exercise.getTitle(),exercise.getDescription(),exercise.getSets(), exercise.getRepetitions(),
                    exercise.getWeight(), exercise.getCategory(), exercise.getMuscleType()));
        }
        return responseCat;
    }

    public ExerciseDTO getExercise(String exerciseTitle){
        Exercise exercise = exerciseRepository.findByTitle(exerciseTitle).get();
        return new ExerciseDTO(exercise.getTitle(),exercise.getDescription(),exercise.getSets(), exercise.getRepetitions(),
                exercise.getWeight(), exercise.getCategory(), exercise.getMuscleType());
    }

}
