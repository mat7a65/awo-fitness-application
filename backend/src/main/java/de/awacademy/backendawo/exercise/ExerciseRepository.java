package de.awacademy.backendawo.exercise;

import de.awacademy.backendawo.workoutPlan.WorkoutPlan;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface ExerciseRepository extends CrudRepository<Exercise, Integer> {
    List<Exercise> findAll();
    List<Exercise> findAllByCategory(String category);
    Optional<Exercise> findByTitle(String title);
}
