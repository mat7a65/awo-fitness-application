package de.awacademy.backendawo;

import de.awacademy.backendawo.exercise.Exercise;
import de.awacademy.backendawo.exercise.ExerciseRepository;
import de.awacademy.backendawo.user.User;
import de.awacademy.backendawo.user.UserRepository;
import de.awacademy.backendawo.userexercise.UserExercise;
import de.awacademy.backendawo.userexercise.UserExerciseRepository;
import de.awacademy.backendawo.workoutPlan.WorkoutPlan;
import de.awacademy.backendawo.workoutPlan.WorkoutPlanRepository;
import de.awacademy.backendawo.workoutSession.WorkoutSession;
import de.awacademy.backendawo.workoutSession.WorkoutSessionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import org.springframework.security.crypto.password.PasswordEncoder;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;

@Component
public class SetupComponent {
    private final ExerciseRepository exerciseRepository;
    private final UserRepository userRepository;
    private final PasswordEncoder passwordEncoder;
    private final WorkoutSessionRepository workoutSessionRepository;
    private final WorkoutPlanRepository workoutPlanRepository;
    private final UserExerciseRepository userExerciseRepository;

    @Autowired
    public SetupComponent(ExerciseRepository exerciseRepository, WorkoutSessionRepository workoutSessionRepository, UserRepository userRepository, PasswordEncoder passwordEncoder, WorkoutPlanRepository workoutPlanRepository, UserExerciseRepository userExerciseRepository) {
        this.exerciseRepository = exerciseRepository;
        this.workoutSessionRepository = workoutSessionRepository;
        this.userRepository = userRepository;
        this.passwordEncoder = passwordEncoder;
        this.workoutPlanRepository = workoutPlanRepository;
        this.userExerciseRepository = userExerciseRepository;
    }


    @EventListener
    @Transactional
    public void handleApplicationReady(ApplicationReadyEvent event) {
        if (userRepository.count() == 0) {
            userRepository.save(new User("Franz", passwordEncoder.encode("passwort")));
        }

        if (exerciseRepository.count() == 0) {

            /////////////////////////////////// Übungen Kraft ///////////////////////////////////
            exerciseRepository.save(new Exercise("Butterfly", "Setzte dich auf den Sitz der " +
                    "Butterflymaschine, die Füße sollen dabei guten Kontakt mit dem Boden machen, die Oberarme " +
                    "sind waagerecht zum Boden. Drücke nun die Gewichte zusammen, wobei die Griffe sich nicht " +
                    "berühren sollen. Gehe nun wieder zurück, das Trainingsgewicht darf dabei nicht abgelegt werden.",
                    4, 12, 0, "Kraft", "Brust"));
            exerciseRepository.save(new Exercise("Bankdrücken", "Lege dich auf die Bank, " +
                    "die Stange direkt über die Augen, die Knie etwas angewinkelt und die Füße fest auf dem Boden. " +
                    "Greife die Stange breit und lasse sie langsam und kontrolliert runter, dabei sollte die " +
                    "Stange kurz auf Brustwarzenhöhe den Körper berühren. Dann das Gewicht wieder hochdrücken " +
                    "bis die Arme durchgestreckt sind. Bei hohem Gewicht, empfiehlt sich natürlich einen Spotter " +
                    "zu haben, der einen hilft falls man die Stange nicht alleine hochdrücken kann. " +
                    "Mit der Breite des Griffs kann außerdem kontrolliert werden, welcher Bereich " +
                    "der Brust stärker belastet wird",
                    4, 12, 0, "Kraft", "Brust"));
            exerciseRepository.save(new Exercise("Kreuzheben", "Stelle dich mit etwas mehr als " +
                    "schulterbreitem Stand vor der Stange, die Füße zeigen leicht nach außen, die Stange ist " +
                    "direkt darüber und sehr nahe am Schienbein. Beuge die Knie (zeigen ebenfalls etwas nach außen)" +
                    " und neige den Oberkörper (bleibt während der ganzen Übung gerade). " +
                    "Greife die Stange schulterbreit mit einem Unter- und einem Obergriff. " +
                    "Ziehe nun die Stange nach oben. An der höchsten Stelle mache ein leichtes Hohlkreuz " +
                    "und drücke die Schultern nach hinten. Gehe wieder runter, wobei du darauf achtest, " +
                    "dass der Rücken gerade bleibt und sich nicht krümmt. Du kannst unten angekommen " +
                    "eine kleine Pause einlegen oder sofort weitermachen.",
                    4, 12, 0, "Kraft", "Rücken"));
            exerciseRepository.save(new Exercise("Long-Pulley", "Setze dich hin, stelle die Füße " +
                    "auf die Stützstellen ab und greife die Stange mit breitem Griff. Ziehe nun die Hände" +
                    " auf den Nabel, nicht höher. Dabei sollten die Arme nahe am Körper bleiben und " +
                    "die Schultern so weit wie möglich zusammengezogen werden. " +
                    "Lasse langsam und kontrolliert das Gewicht wieder nach vorne bis die " +
                    "Arme vollständig ausgestreckt sind.",
                    4, 12, 0, "Kraft", "Rücken"));
            exerciseRepository.save(new Exercise("Latzug Zur Brust", "Aufrechte Sitzposition Oberkörper " +
                    "leicht nach hinten beugen Stange im Obergriff fassen Stange vor dem Kopf nach unten zum " +
                    "Brustbein ziehen Stange zurückführen, bis Ellenbogen leicht gebeugt.",
                    4, 12, 0, "Kraft", "Rücken"));
            exerciseRepository.save(new Exercise("Beinstrecker", "Setze dich in die " +
                    "Beinstreckermaschine. Der Rücken soll einen festen Halt haben, stelle die Maschine so ein, " +
                    "dass du festsitzt. In der Anfangsposition sind die Beine etwas nach hinten angewinkelt. " +
                    "Drücke das Gewicht nach oben bis die Beine nahezu vollständig ausgestreckt sind. " +
                    "Gehe langsam und kontrolliert wieder runter.",
                    4, 20, 0, "Kraft", "Beine"));
            exerciseRepository.save(new Exercise("Kniebeuge", "Stelle die Halterung der Langhantel " +
                    "auf so eine Höhe ein, dass du sie bequem raus- und wieder reinbringen kannst. Bereite dich vor:\n" +
                    "\n" +
                    "die Stange ist etwas tiefer als Schulterhöhe\n" +
                    "\n" +
                    "die Füße sind ziemlich auseinander und zeigen leicht nach außen\n" +
                    "\n" +
                    "der Kopf ist im Nacken und schaut nach vorne/oben.\n" +
                    "\n" +
                    "die Brust wird nach außen gebracht\n" +
                    "\n" +
                    "Gehe nun langsam runter, bis der Oberschenkel einen rechten Winkel bildet, nicht tiefer.\n" +
                    "\n" +
                    "Die Knie zeigen leicht nach außen, dein Gesäß nach hinten. Mache eine kleine Pause und gehe mit " +
                    "so viel Energie wie du aufbringen kannst, wieder nach oben.\n" +
                    "\n" +
                    "Nach 2 Sekunden Pause gehe wieder runter",
                    4, 12, 0, "Kraft", "Beine"));
            exerciseRepository.save(new Exercise("Crunches", "Lege dich auf eine Matte mit " +
                    "angewinkelten Beinen. Die Füße werden irgendwie festgehalten (Partner, Lanhghantel, o.Ä.)" +
                    " und die Hände werden hinter dem Nacken verschränkt. Aus dieser Position führe den Oberkörper" +
                    " so weit nach oben, bis Kopf oder Ellenbogen die angewinkelten Beine berühren. " +
                    "Es ist wichtig, dass dieser Vorgang mit einer rollenden Bewegung durchgeführt wird: " +
                    "die Wirbelsäule sollte sich Wirbel für Wirbel von der Matte lösen. " +
                    "Ein Hohlkreuz ist stets zu vermeiden.",
                    4, 20, 0, "Kraft", "Bauch"));
            exerciseRepository.save(new Exercise("Arnold Press", "Zwei Kurzhanteln mit den " +
                    "Handflächen nach innen vor der Brust halten, Ellbogen unterhalb der Handgelenke.\n" +
                    "\n" +
                    "Ellbogen nach außen bringen und Hanteln über den Kopf heben. Drehung kommt aus dem " +
                    "Ellbogen, nicht aus den Armen!\n" +
                    "\n" +
                    "Wenn die Arme durchgestreckt sind, die gleiche Bewegung rückwärts ausführen.",
                    4, 12, 0, "Kraft", "Schultern"));
            exerciseRepository.save(new Exercise("Seitheben", "Stelle dich mit schulterbreiten" +
                    " Beinen und leicht nachaußen zeigende Füße mit zwei Hanteln auf. " +
                    "Die Arme sind fast vollständig ausgestreckt. Hebe nun die Hanteln seitlich nach oben,\n" +
                    "\n" +
                    "auf dem höchsten Punkt kannst du eine kurze (1-2 sek.) Pause machen. Achte darauf, " +
                    "dass die Handflächen sich während der Bewegung nicht drehen, sie sollten also " +
                    "immer zum Boden oder zum Körper zeigen.",
                    4, 12, 0, "Kraft", "Schultern"));

            exerciseRepository.save(new Exercise("Hyperextensions", "Lege dich auf das Polster so, dass der " +
                    "Bauchnabel kurz vor der Vorderkante liegt, der Oberkörper hängt frei nach unten. " +
                    "Spanne die gesamte Rückenmuskulatur an, und bringe den Oberkörper nach oben bis er " +
                    "waagerecht ist (nicht höher). Gehe nun langsam wieder nach unten " +
                    "(Muskelanspannung nicht vergessen).",
                    4, 20, 0, "Kraft", "Rücken"));

            /////////////////////////////////// Übungen Ausdauer ///////////////////////////////////

            exerciseRepository.save(new Exercise("Springseil", "Du stellst dich aufrecht hin." +
                    " Hände auf Hüfthöhe, dabei sollte Springseil leicht auf dem Boden aufliegen." +
                    " Bei springen darauf achten möglichst kleine Sprünge zu machen und Arme und" +
                    " Oberkörper so gering wie möglich zu bewegen.",
                    20, 0, "Ausdauer", "Ganzkörper"));

            exerciseRepository.save(new Exercise("Bergsteiger", "Du stütz dich auf deinen Händen ab." +
                    " Strecke das rechte Bein nach hinten aus, beuge dein linkes Bein nach vorne," +
                    " mit dem Knie auf Brusthöhe. Mache einen kleinen Sprung und bring dabei gleichzeitig" +
                    " dein rechtes Bein nach vorne, während Du dein linkes Bein nach hinten streckst." +
                    " Diese Übung wechselseitig ausführen und darauf achten das der Körper gerade bleibt.",
                    20, 0, "Ausdauer", "Beine"));

            exerciseRepository.save(new Exercise("Laufen", "Beim Laufen auf aufrechten Oberkörper" +
                    " und regelmäßige Atmung achten. Lauftempo am Anfang lieber zu langsam als zu schnell." +
                    " Nicht auspowern und am Ende einen cool down mit langsamen gehen für mind. 5 Minuten einplanen.",
                    20, 0, "Ausdauer", "Ganzkörper"));

            exerciseRepository.save(new Exercise("Jumping Jacks", "Aufrecht hinstellen," +
                    " Arme sind entspannt an den Körperseiten. Die Beine stehen in der Ausgangsposition" +
                    " eng beisammen. Mit leichter Beugung im Knie springst du nach oben ab und spreizt" +
                    " deine Beine etwas breiter als schulterweit. Während des Sprungs in die Breite hebst" +
                    " du die Hände über den Kopf. Anschließend springst du zurück in die Ausgangsposition" +
                    " (enger Stand) und senkst die Arme wieder zu deinen Körperseiten.",
                    10, 0, "Ausdauer", "Ganzkörper"));

            exerciseRepository.save(new Exercise("Radfahren", "Beim Radfahren ein Fahrrad" +
                    " geeigneter Größe und Einstellungen wählen. Auf regelmäßige Atmung achten." +
                    " Tempo am Anfang lieber zu langsam als zu schnell. Nicht auspowern und am" +
                    " Ende einen cool down mit langsamen fahren für mind. 10 Minuten absolvieren.",
                    60, 0, "Ausdauer", "Ganzkörper"));

            exerciseRepository.save(new Exercise("Kniehebelauf", "Du läufst auf der Stelle und" +
                    " hebst die Knie bis auf Hüfthöhe hoch (ungefähr 90 Grad). Wichtig ist, die Arme mitzunehmen!",
                    15, 0, "Ausdauer", "Ganzkörper"));


            /////////////////////////////////// Übungen Abnehmen ///////////////////////////////////

            exerciseRepository.save(new Exercise("Plank", "Die Plank (Unterarmstütz)" +
                    " ist eine statische Übung, für die nur das Körpergewicht nötig ist." +
                    " Die klassische Plank ist wie ein Liegestütz, nur dass der Oberkörper auf" +
                    " die Unterarme gestützt ist. Die Ellenbogen sind schulterbreit auseinander," +
                    " die Unterarme parallel zueinander – alternativ können sich die Hände aber auch berühren." +
                    " Der ganze Körper ist angespannt, nur Zehen und Unterarme berühren den Boden. Achte darauf," +
                    " dass der Bauch oder die Hüfte nicht durchhängt.\n" +
                    "\n" +
                    "Versuche, die Plank für 30 Sekunden zu halten und dich mit der Zeit zu steigern.",
                    10, 0, "Abnehmen", "Ganzkörper"));

            exerciseRepository.save(new Exercise("Käfer", "Lege dich auf den Rücken und" +
                    " strecke die Beine gerade nach unten aus. Lege die Fingerspitzen an deinen Hinterkopf," +
                    " die Ellenbogen zeigen nach rechts und links. Nun führe abwechselnd den" +
                    " linken Ellenbogen an das rechte Knie und den rechten Ellenbogen an das linke Knie",
                    10, 0, "Abnehmen", "Beine"));

            exerciseRepository.save(new Exercise("Hüftheben", "Legt euch für diese Übung" +
                    " auf den Rücken, die Arme liegen neben eurem Körper auf dem Boden. Stellt die Beine auf," +
                    " sodass eure Fußsohlen komplett auf dem Boden aufliegen. Jetzt drückt ihr euer Becken" +
                    " langsam mit Kraft aus dem Po und der Oberschenkelrückseite hoch, bis eure Oberschenkel" +
                    " und euer Oberkörper eine Gerade bilden. Ihr kennt die Position vielleicht als Brücke." +
                    " Haltet sie einen Moment und senkt euer Becken dann langsam und kontrolliert wieder ab." +
                    " Legt es aber nicht auf dem Boden ab, sondern stoppt kurz davor und wiederholt die Ausführung.",
                    10, 0, "Abnehmen", "Beine"));

            exerciseRepository.save(new Exercise("Liegestütze", "Diese Übung beginnst" +
                    " Du im Liegen auf dem Bauch, die Hände stützen Sie auf Schulterhöhe auf, etwas weiter" +
                    " als schulterbreit. Die Füße stehen parallel. Nun stemmst Du dich hoch, bis die Arme" +
                    " ausgestreckt sind. Hierbei sollten die Ellenbogen eher nach außen zeigen. Der Körper" +
                    " sollte möglichst eine gerade Linie ergeben.\n" +
                    "\n" +
                    "Die Arme dann wieder anwinkeln und den Körper absenken – im Optimalfall, bis die Nase den" +
                    " Boden berührt. Für Einsteiger kann die Übung auch auf den Knien ausgeführt werden.",
                    15, 0, "Abnehmen", "Oberkörper"));

            exerciseRepository.save(new Exercise("Dips", "Stützt euch mit den Händen am" +
                    " Dips-Barren ab – alternativ an der Kante einer Hantelbank, wenn ihr kein Gerät für" +
                    " Dips habt bzw. das gerade besetzt ist. Die Arme sind nicht komplett gestreckt," +
                    " die Schultern bleiben weg von den Ohren. Die Beine könnt ihr im rechten Winkel beugen" +
                    " und überkreuzen, das ist angenehmer. Jetzt spannt ihr den Rumpf fest an und lasst den" +
                    " Oberkörper kontrolliert zwischen den Barren ab, indem ihr die Ellbogen beugt. Führt die" +
                    " Ellbogen und Oberarme nah am Körper entlang. Wenn euer Oberarm etwa parallel zum Boden" +
                    " ist und die Ellbogen spitz nach hinten herausstehen, seid ihr in der maximalen unteren" +
                    " Position angekommen und drückt euch langsam wieder hoch in die Ausgangsposition." +
                    " Der Oberkörper bleibt die ganze Zeit aufrecht und möglichst unbewegt.",
                    10, 0, "Abnehmen", "Oberkörper"));

        }

        if (workoutPlanRepository.count() == 0) {

            /////////////////////////////////// Einmal Die Woche trainieren ///////////////////////////////////
            WorkoutPlan workoutPlan1MalDieWoche = new WorkoutPlan("Ganzkörpertraining");
            workoutPlanRepository.save(workoutPlan1MalDieWoche);

            /////////////////////////////////// Zweimal Die Woche Kraftraining ///////////////////////////////////
            WorkoutPlan workoutPlan2MalDieWocheKraft = new WorkoutPlan("Krafttraining");
            workoutPlanRepository.save(workoutPlan2MalDieWocheKraft);

            /////////////////////////////////// Viermal Die Woche Ausdauertraining ///////////////////////////////////
            WorkoutPlan workoutPlan4MalDieWocheAusdauer = new WorkoutPlan("Ausdauertraining");
            workoutPlanRepository.save(workoutPlan4MalDieWocheAusdauer);

            /////////////////////////////////// Zweimal Die Woche Abnehmtraining ///////////////////////////////////
            WorkoutPlan workoutPlan2MalDieWocheAbnehmen = new WorkoutPlan("Abnehmtraining");
            workoutPlanRepository.save(workoutPlan2MalDieWocheAbnehmen);


            if (workoutSessionRepository.count() == 0) {

                /////////////////////////////////// Session Kraft ///////////////////////////////////
                WorkoutSession workoutSession1Ganzkoerper = new WorkoutSession(workoutPlan1MalDieWoche, "Ganzkoerper");
                workoutSession1Ganzkoerper.addExercise(exerciseRepository.findByTitle("Bankdrücken").get());
                workoutSession1Ganzkoerper.addExercise(exerciseRepository.findByTitle("Beinstrecker").get());
                workoutSession1Ganzkoerper.addExercise(exerciseRepository.findByTitle("Arnold Press").get());
                workoutSession1Ganzkoerper.addExercise(exerciseRepository.findByTitle("Long-Pulley").get());
                workoutSession1Ganzkoerper.addExercise(exerciseRepository.findByTitle("Crunches").get());
                workoutSessionRepository.save(workoutSession1Ganzkoerper);

                WorkoutSession workoutSession1Oberkoerper = new WorkoutSession(workoutPlan2MalDieWocheKraft, "Oberkörper");
                workoutSession1Oberkoerper.addExercise(exerciseRepository.findByTitle("Bankdrücken").get());
                workoutSession1Oberkoerper.addExercise(exerciseRepository.findByTitle("Butterfly").get());
                workoutSession1Oberkoerper.addExercise(exerciseRepository.findByTitle("Latzug Zur Brust").get());
                workoutSession1Oberkoerper.addExercise(exerciseRepository.findByTitle("Arnold Press").get());
                workoutSession1Oberkoerper.addExercise(exerciseRepository.findByTitle("Seitheben").get());
                workoutSessionRepository.save(workoutSession1Oberkoerper);

                WorkoutSession workoutSession2Unterkoerper = new WorkoutSession(workoutPlan2MalDieWocheKraft, "Unterkörper");
                workoutSession2Unterkoerper.addExercise(exerciseRepository.findByTitle("Kniebeuge").get());
                workoutSession2Unterkoerper.addExercise(exerciseRepository.findByTitle("Beinstrecker").get());
                workoutSession2Unterkoerper.addExercise(exerciseRepository.findByTitle("Crunches").get());
                workoutSession2Unterkoerper.addExercise(exerciseRepository.findByTitle("Hyperextensions").get());
                workoutSessionRepository.save(workoutSession2Unterkoerper);


                /////////////////////////////////// Session Ausdauer ///////////////////////////////////
                WorkoutSession workoutSession1Ausdauer = new WorkoutSession(workoutPlan4MalDieWocheAusdauer, "Ausdauer1");
                workoutSession1Ausdauer.addExercise(exerciseRepository.findByTitle("Radfahren").get());
                workoutSessionRepository.save(workoutSession1Ausdauer);

                WorkoutSession workoutSession2Ausdauer = new WorkoutSession(workoutPlan4MalDieWocheAusdauer, "Ausdauer2");
                workoutSession2Ausdauer.addExercise(exerciseRepository.findByTitle("Laufen").get());
                workoutSessionRepository.save(workoutSession2Ausdauer);

                WorkoutSession workoutSession3Ausdauer = new WorkoutSession(workoutPlan4MalDieWocheAusdauer, "Ausdauer3");
                workoutSession3Ausdauer.addExercise(exerciseRepository.findByTitle("Jumping Jacks").get());
                workoutSession3Ausdauer.addExercise(exerciseRepository.findByTitle("Laufen").get());
                workoutSessionRepository.save(workoutSession3Ausdauer);

                WorkoutSession workoutSession4Ausdauer = new WorkoutSession(workoutPlan4MalDieWocheAusdauer, "Ausdauer4");
                workoutSession4Ausdauer.addExercise(exerciseRepository.findByTitle("Bergsteiger").get());
                workoutSession4Ausdauer.addExercise(exerciseRepository.findByTitle("Springseil").get());
                workoutSession4Ausdauer.addExercise(exerciseRepository.findByTitle("Laufen").get());
                workoutSessionRepository.save(workoutSession4Ausdauer);

                /////////////////////////////////// Session Abnehmen ///////////////////////////////////
                WorkoutSession workoutSession1Abnehmen = new WorkoutSession(workoutPlan2MalDieWocheAbnehmen, "Abnehmen1");
                workoutSession1Abnehmen.addExercise(exerciseRepository.findByTitle("Radfahren").get());
                workoutSession1Abnehmen.addExercise(exerciseRepository.findByTitle("Plank").get());
                workoutSession1Abnehmen.addExercise(exerciseRepository.findByTitle("Hüftheben").get());
                workoutSession1Abnehmen.addExercise(exerciseRepository.findByTitle("Dips").get());
                workoutSessionRepository.save(workoutSession1Abnehmen);


                WorkoutSession workoutSession2Abnehmen = new WorkoutSession(workoutPlan2MalDieWocheAbnehmen, "Abnehmen2");
                workoutSession2Abnehmen.addExercise(exerciseRepository.findByTitle("Liegestütze").get());
                workoutSession2Abnehmen.addExercise(exerciseRepository.findByTitle("Plank").get());
                workoutSession2Abnehmen.addExercise(exerciseRepository.findByTitle("Käfer").get());
                workoutSession2Abnehmen.addExercise(exerciseRepository.findByTitle("Kniehebelauf").get());
                workoutSessionRepository.save(workoutSession2Abnehmen);

            }
        }

        User testUser = userRepository.findByUserName("Franz").get();
        WorkoutPlan testPlan = workoutPlanRepository.findByTitle("Krafttraining").get();
        testPlan.setUser(testUser);
        workoutPlanRepository.save(testPlan);

        for(User user : userRepository.findAll()){
            for(WorkoutPlan workoutPlan : workoutPlanRepository.findAllByUserUserId(user.getUserId())){
                for (WorkoutSession workoutSession : workoutSessionRepository.findAllByWorkoutPlanWorkoutPlanId(workoutPlan.getWorkoutPlanId())){
                    for(Exercise exercise : workoutSession.getExercises()){
                        userExerciseRepository.save(new UserExercise(user, exercise, exercise.getSets(), exercise.getRepetitions()));
                    }
                }
            }
        }
    }
}
