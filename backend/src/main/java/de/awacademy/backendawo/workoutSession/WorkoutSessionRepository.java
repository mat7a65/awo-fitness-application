package de.awacademy.backendawo.workoutSession;

import de.awacademy.backendawo.exercise.Exercise;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface WorkoutSessionRepository extends CrudRepository<WorkoutSession, Integer> {
    List<WorkoutSession> findAll();
    List<WorkoutSession> findAllByWorkoutPlanWorkoutPlanId(Integer id);
    List<WorkoutSession> findAllByWorkoutPlanTitle(String title);
    Optional<WorkoutSession> findByWorkoutSessionId(Integer id);

}
