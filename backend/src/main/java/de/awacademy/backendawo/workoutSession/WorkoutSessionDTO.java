package de.awacademy.backendawo.workoutSession;

import de.awacademy.backendawo.exercise.Exercise;

import java.util.LinkedList;
import java.util.List;
import java.util.Set;

public class WorkoutSessionDTO {

    private String title;
    private List<Exercise> exercises;

    public WorkoutSessionDTO(String title, List<Exercise> exercises) {
        this.title = title;
        this.exercises = exercises;
    }

    public List<Exercise> getExercises() {
        return exercises;
    }

    public void setExercises(List<Exercise> exercises) {
        this.exercises = exercises;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
