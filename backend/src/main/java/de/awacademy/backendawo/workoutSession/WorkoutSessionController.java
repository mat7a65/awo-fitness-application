package de.awacademy.backendawo.workoutSession;

import de.awacademy.backendawo.workoutPlan.WorkoutPlanDTO;
import de.awacademy.backendawo.workoutPlan.WorkoutPlanService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class WorkoutSessionController {

    @Autowired
    private WorkoutSessionService workoutSessionService;

    public WorkoutSessionController(WorkoutSessionService workoutSessionService) {
        this.workoutSessionService = workoutSessionService;
    }
    @GetMapping("/api/workoutsession/{workoutPlan}")
    public List<WorkoutSessionDTO> view(@PathVariable String workoutPlan) {
        return workoutSessionService.workoutSessionList(workoutPlan);
    }
}
