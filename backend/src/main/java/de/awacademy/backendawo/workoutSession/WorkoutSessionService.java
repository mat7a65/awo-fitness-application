package de.awacademy.backendawo.workoutSession;

import de.awacademy.backendawo.workoutPlan.WorkoutPlan;
import de.awacademy.backendawo.workoutPlan.WorkoutPlanDTO;
import de.awacademy.backendawo.workoutPlan.WorkoutPlanRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

@Service
public class WorkoutSessionService {

    @Autowired
    private WorkoutSessionRepository workoutSessionRepository;

    public List<WorkoutSessionDTO> workoutSessionList(String title){
        List<WorkoutSessionDTO> response = new ArrayList<>();

        for (WorkoutSession workoutSession : workoutSessionRepository.findAllByWorkoutPlanTitle(title)) {
            response.add(new WorkoutSessionDTO(workoutSession.getTitle(), workoutSession.getExercises()));
        }
        return response;
    }
}
