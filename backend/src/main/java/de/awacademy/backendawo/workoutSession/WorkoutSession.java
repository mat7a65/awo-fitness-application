package de.awacademy.backendawo.workoutSession;

import de.awacademy.backendawo.exercise.Exercise;
import de.awacademy.backendawo.workoutPlan.WorkoutPlan;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Entity
public class WorkoutSession {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int workoutSessionId;

    private String title;

    private boolean done;

    @ManyToOne
    private WorkoutPlan workoutPlan;

    @ManyToMany(cascade = {
            CascadeType.PERSIST,
            CascadeType.MERGE
    })
//    @JoinTable(
//            joinColumns = @JoinColumn(name = "id"),
//            inverseJoinColumns = @JoinColumn(name = "exercise_id")
//    )
    private List<Exercise> exercises = new ArrayList<>();

    public List<Exercise> getExercises() {
        return exercises;
    }

    public void addExercise(Exercise exercise){
        exercises.add(exercise);
    }

    public void setExercises(List<Exercise> exercises) {
        this.exercises = exercises;
    }

    public WorkoutSession() {
    }

    public WorkoutSession(WorkoutPlan workoutplan, String title) {
        this.workoutPlan = workoutplan;
        this.title = title;
    }

    public String getTitle() {
        return title;
    }

    public int getWorkoutSessionId() {
        return workoutSessionId;
    }

    public void setWorkoutSessionId(int workoutSessionId) {
        this.workoutSessionId = workoutSessionId;
    }

    public String getImpression() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public boolean isDone() {
        return done;
    }

    public void setDone(boolean done) {
        this.done = done;
    }

    public WorkoutPlan getWorkoutPlan() {
        return workoutPlan;
    }

    public void setWorkoutPlan(WorkoutPlan workoutPlan) {
        this.workoutPlan = workoutPlan;
    }

}

