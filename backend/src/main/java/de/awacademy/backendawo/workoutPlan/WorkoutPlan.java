package de.awacademy.backendawo.workoutPlan;

import de.awacademy.backendawo.exercise.Exercise;
import de.awacademy.backendawo.user.User;
import de.awacademy.backendawo.workoutSession.WorkoutSession;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

@Entity
public class WorkoutPlan {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int workoutPlanId;

    private String title;

    private int workoutWeek;

    private int workoutDay;

    @OneToMany(mappedBy = "workoutPlan")
    private List<WorkoutSession> workoutSessionList;

    @ManyToOne
    private User user;

    public WorkoutPlan() {
    }

    public WorkoutPlan(String title) {
        this.title = title;
    }

    public void addWorkoutSession(WorkoutSession workoutSession) {
        workoutSessionList.add(workoutSession);
    }

    public int getWorkoutPlanId() {
        return workoutPlanId;
    }

    public void setWorkoutPlanId(int workoutPlanId) {
        this.workoutPlanId = workoutPlanId;
    }

    public int getWorkoutWeek() {
        return workoutWeek;
    }

    public void setWorkoutWeek(int workoutWeek) {
        this.workoutWeek = workoutWeek;
    }

    public int getWorkoutDay() {
        return workoutDay;
    }

    public void setWorkoutDay(int workoutDay) {
        this.workoutDay = workoutDay;
    }

    public List<WorkoutSession> getWorkoutSessionList() {
        return workoutSessionList;
    }

    public void setWorkoutSessionList(List<WorkoutSession> workoutSessionList) {
        this.workoutSessionList = workoutSessionList;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }


}
