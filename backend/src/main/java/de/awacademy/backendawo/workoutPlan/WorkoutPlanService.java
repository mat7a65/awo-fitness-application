package de.awacademy.backendawo.workoutPlan;

import de.awacademy.backendawo.exercise.Exercise;
import de.awacademy.backendawo.exercise.ExerciseDTO;
import de.awacademy.backendawo.exercise.ExerciseRepository;
import de.awacademy.backendawo.user.User;
import de.awacademy.backendawo.user.UserRepository;
import de.awacademy.backendawo.userexercise.UserExercise;
import de.awacademy.backendawo.userexercise.UserExerciseRepository;
import de.awacademy.backendawo.workoutSession.WorkoutSession;
import de.awacademy.backendawo.workoutSession.WorkoutSessionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

@Service
public class WorkoutPlanService {

    @Autowired
    private WorkoutPlanRepository workoutPlanRepository;

    @Autowired
    private UserExerciseRepository userExerciseRepository;

    @Autowired
    private UserRepository userRepository;

    public List<WorkoutPlanDTO> workoutPlanList (String userName){
        List<WorkoutPlanDTO> response = new ArrayList<>();

        for (WorkoutPlan workoutPlan : workoutPlanRepository.findAllByUserUserName(userName)) {
            response.add(new WorkoutPlanDTO(workoutPlan.getTitle(),workoutPlan.getWorkoutWeek(),workoutPlan.getWorkoutDay()));
        }
        return response;
    }

    public List<WorkoutPlanDTO> workoutPlanList (){
        List<WorkoutPlanDTO> response = new ArrayList<>();

        for (WorkoutPlan workoutPlan : workoutPlanRepository.findAll()) {
            String name;

            if (workoutPlan.getUser() != null) {
                name = workoutPlan.getUser().getUserName();
            }
            else name = "none";

            response.add(new WorkoutPlanDTO(workoutPlan.getTitle(), name));
        }
        return response;
    }

    public List<WorkoutPlanDTO> updateCounter(String userName, String workoutPlanTitle){
        List<WorkoutPlanDTO> response = new ArrayList<>();

        for (WorkoutPlan workoutPlan : workoutPlanRepository.findAllByUserUserName(userName)) {
            if (workoutPlan.getTitle().equals(workoutPlanTitle)){
                int daysPerWeek = workoutPlan.getWorkoutSessionList().size();
                int currentDay = workoutPlan.getWorkoutDay();
                int currentWeek = workoutPlan.getWorkoutWeek();
                if (currentWeek <10) {
                    if (currentDay < (daysPerWeek - 1)) {
                        workoutPlan.setWorkoutDay(currentDay + 1);
                    } else {
                        workoutPlan.setWorkoutDay(0);
                        workoutPlan.setWorkoutWeek(currentWeek + 1);
                    }
                }
                workoutPlanRepository.save(workoutPlan);
            }
            response.add(new WorkoutPlanDTO(workoutPlan.getTitle(),workoutPlan.getWorkoutWeek(),workoutPlan.getWorkoutDay()));
        }
        return response;
    }

    public List<WorkoutPlanDTO> resetCounter(String userName, String workoutPlanTitle){
        List<WorkoutPlanDTO> response = new ArrayList<>();

        for (WorkoutPlan workoutPlan : workoutPlanRepository.findAllByUserUserName(userName)) {
            if (workoutPlan.getTitle().equals(workoutPlanTitle)){
                    workoutPlan.setWorkoutDay(0);
                    workoutPlan.setWorkoutWeek(0);
                workoutPlanRepository.save(workoutPlan);
            }
            response.add(new WorkoutPlanDTO(workoutPlan.getTitle(),workoutPlan.getWorkoutWeek(),workoutPlan.getWorkoutDay()));
        }
        return response;
    }


    public void setUser(String userName, String workoutPlanTitle) {
        User user = userRepository.findByUserName(userName).get();
        WorkoutPlan plan = workoutPlanRepository.findByTitle(workoutPlanTitle).get();
        plan.setUser(user);
        workoutPlanRepository.save(plan);
        for (WorkoutSession workoutSession : plan.getWorkoutSessionList()) {
            for (Exercise exercise : workoutSession.getExercises()) {
                if (!(userExerciseRepository.findByUser_UserIdAndExercise_ExerciseId(user.getUserId(), exercise.getExerciseId()).isPresent())) {
                    userExerciseRepository.save(new UserExercise(user, exercise, exercise.getSets(), exercise.getRepetitions()));
                }
            }
        }

    }

    public void deleteUser(String userName, String workoutPlanTitle){
        WorkoutPlan plan = workoutPlanRepository.findByTitle(workoutPlanTitle).get();
        plan.setWorkoutDay(0);
        plan.setWorkoutWeek(0);
        plan.setUser(null);
        workoutPlanRepository.save(plan);
    }
}
