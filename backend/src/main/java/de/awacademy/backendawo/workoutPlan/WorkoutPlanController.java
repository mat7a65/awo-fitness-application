package de.awacademy.backendawo.workoutPlan;


import de.awacademy.backendawo.workoutSession.WorkoutSession;
import de.awacademy.backendawo.workoutSession.WorkoutSessionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import de.awacademy.backendawo.exercise.ExerciseDTO;
import de.awacademy.backendawo.exercise.ExerciseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import org.springframework.web.bind.annotation.GetMapping;

import java.util.LinkedList;
import java.util.List;
import java.util.Optional;

import java.util.List;

@RestController
public class WorkoutPlanController {

    @Autowired
    private WorkoutPlanService workoutPlanService;

    public WorkoutPlanController(WorkoutPlanService workoutPlanService) {
        this.workoutPlanService = workoutPlanService;
    }

    @GetMapping("/api/workoutplan/{userName}")
    public List<WorkoutPlanDTO> viewDetails(@PathVariable String userName) {
        return workoutPlanService.workoutPlanList(userName);
    }

    @GetMapping("/api/workoutplan/")
    public List<WorkoutPlanDTO> view() {
        return workoutPlanService.workoutPlanList();
    }

    @PostMapping("/api/workoutplan/{userName}")
    public List<WorkoutPlanDTO> updateCounter(@PathVariable String userName, @RequestBody String workoutPlanTitle) {
        return workoutPlanService.updateCounter(userName, workoutPlanTitle);
    }

    @PostMapping("/api/resetWorkoutplan/{userName}")
    public List<WorkoutPlanDTO> resetCounter(@PathVariable String userName, @RequestBody String workoutPlanTitle) {
        return workoutPlanService.resetCounter(userName, workoutPlanTitle);
    }

    @PostMapping("/api/setWorkoutplan/{userName}")
    public void setPlan(@PathVariable String userName, @RequestBody String workoutPlanTitle) {
        workoutPlanService.setUser(userName, workoutPlanTitle);
    }

    @PostMapping("/api/deleteWorkoutplan/{userName}")
    public List<WorkoutPlanDTO> deletePlan(@PathVariable String userName, @RequestBody String workoutPlanTitle) {
        workoutPlanService.deleteUser(userName, workoutPlanTitle);
        return workoutPlanService.workoutPlanList(userName);
    }

}
