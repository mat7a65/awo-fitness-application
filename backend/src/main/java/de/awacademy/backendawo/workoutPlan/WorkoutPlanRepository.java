package de.awacademy.backendawo.workoutPlan;

import de.awacademy.backendawo.user.User;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface WorkoutPlanRepository extends CrudRepository<WorkoutPlan, Integer> {
    List<WorkoutPlan> findAll();
    List<WorkoutPlan> findAllByUserUserId(Integer id);
    List<WorkoutPlan> findAllByUserUserName(String userName);
    Optional<WorkoutPlan> findByTitle(String title);
    Optional<User> findByUser_UserId(Integer userId);

}
