package de.awacademy.backendawo.workoutPlan;

import de.awacademy.backendawo.user.User;
import de.awacademy.backendawo.workoutSession.WorkoutSession;

import javax.persistence.OneToMany;
import java.util.List;

public class WorkoutPlanDTO {

    private String title;

    private int workoutWeek;

    private int workoutDay;

    private String  username;

    public WorkoutPlanDTO(String title, int workoutWeek, int workoutDay) {
        this.title = title;
        this.workoutWeek = workoutWeek;
        this.workoutDay = workoutDay;
    }

    public WorkoutPlanDTO(String title, String username) {
        this.title = title;
        this.username = username;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public WorkoutPlanDTO(String title) {
        this.title = title;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getWorkoutWeek() {
        return workoutWeek;
    }

    public void setWorkoutWeek(int workoutWeek) {
        this.workoutWeek = workoutWeek;
    }

    public int getWorkoutDay() {
        return workoutDay;
    }

    public void setWorkoutDay(int workoutDay) {
        this.workoutDay = workoutDay;
    }
}
