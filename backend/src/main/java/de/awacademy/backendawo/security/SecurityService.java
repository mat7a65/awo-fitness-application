package de.awacademy.backendawo.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import de.awacademy.backendawo.user.User;
import de.awacademy.backendawo.user.UserRepository;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.LinkedList;
import java.util.List;
import java.util.Optional;

@Service
public class SecurityService implements UserDetailsService {

    private PasswordEncoder passwordEncoder;
    private UserRepository userRepository;

    @Autowired
    public SecurityService(PasswordEncoder passwordEncoder, UserRepository userRepository) {
        this.passwordEncoder = passwordEncoder;
        this.userRepository = userRepository;
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        Optional<User> user= userRepository.findByUserNameIgnoreCase(username);

        List<GrantedAuthority> authorities = new LinkedList<>();

        return new org.springframework.security.core.userdetails.User(user.get().getUserName(), user.get().getUserPassword(), authorities);
    }

    public void saveUser(String username, String password){
        userRepository.save(new User(username, this.passwordEncoder.encode(password)));
    }
}
