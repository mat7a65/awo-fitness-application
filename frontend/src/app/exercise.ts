export interface Exercise {

  title: string;

  description: string;

  sets: number;

  repetitions: number;

  weight: number;

  category: string;

  muscleType: string;

  dauer: number;

}
