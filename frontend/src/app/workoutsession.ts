import {Exercise} from './exercise';

export interface WorkoutSession {
  title;
  exercises: Exercise[];
}
