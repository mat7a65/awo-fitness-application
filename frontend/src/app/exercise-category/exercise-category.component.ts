import {Component} from '@angular/core';
import {Exercise} from '../exercise';
import {HttpClient} from '@angular/common/http';
import { ActivatedRoute} from '@angular/router';


@Component({
  selector: 'app-exercise-category',
  templateUrl: './exercise-category.component.html',
  styleUrls: ['./exercise-category.component.css']
})
export class ExerciseCategoryComponent {

  exercises: Exercise[] = [];
  constructor(private route: ActivatedRoute, private httpService: HttpClient) {
    route.params.subscribe(params => {
      this.getExercises(params.category);
      });
  }
  getExercises(category) {
    this.httpService.get<Exercise[]>('/api/exercisecategory/' + category)
      .subscribe(exercise => this.exercises = exercise);
  }
}

