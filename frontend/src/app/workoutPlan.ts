export interface WorkoutPlan {
  title: string;
  workoutWeek: number;
  workoutDay: number;
  username: string;
}
