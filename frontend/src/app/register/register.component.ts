import { Component, OnInit } from '@angular/core';
import {User} from '../user';
import {HttpClient} from '@angular/common/http';
import {UserDTO} from '../userDTO';
import {Router} from '@angular/router';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
  registerData: UserDTO = {
    username: '', password: ''
  };
  registriert: string;
  constructor(private http: HttpClient, private router: Router) { }

  ngOnInit() {
  }

  register() {
    this.http.post<UserDTO>('/api/register', this.registerData)
      .subscribe(user => this.registriert = 'Du bist registriert ' + user.username);
    this.router.navigate(['/login']);
  }
}
