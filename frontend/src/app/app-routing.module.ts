import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {ExerciseComponent} from './exercise/exercise.component';
import {ExerciseDetailsComponent} from './exercise-details/exercise-details.component';
import {LoginComponent} from './login/login.component';
import {RegisterComponent} from './register/register.component';
import {UserDetailsComponent} from './user-details/user-details.component';
import {WorkoutplanComponent} from './workoutplan/workoutplan.component';
import {WorkoutplanDetailsComponent} from './workoutplan-details/workoutplan-details.component';
import {ExerciseCategoryComponent} from './exercise-category/exercise-category.component';
import {DisclaimerComponent} from './disclaimer/disclaimer.component';
import {UserExerciseComponent} from './user-exercise/user-exercise.component';
import {HomeComponent} from './home/home.component';


const routes: Routes = [
  {
    path: 'exerciseList',
    component: ExerciseComponent,
  },
  {
    path: 'exerciseList/:category',
    component: ExerciseCategoryComponent
  },
  {
    path: 'exerciseDetails/:exerciseTitle',
    component: ExerciseDetailsComponent
  },
  {
    path: 'workoutplanDetails/:workoutTitle',
    component: WorkoutplanDetailsComponent
  },
  {
    path: 'login',
    component: LoginComponent
  },
  {
    path: 'registration',
    component: RegisterComponent
  },
  {
    path: 'overview',
    component: UserDetailsComponent
  },
  {
    path: 'trainingspläne',
    component: WorkoutplanComponent
  },
  {
    path: 'disclaimer',
    component: DisclaimerComponent
  },
  {path: 'userExercise/:userName/:exerciseTitel',
    component: UserExerciseComponent, },
  {
    path: '',
    component: HomeComponent
  },

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
