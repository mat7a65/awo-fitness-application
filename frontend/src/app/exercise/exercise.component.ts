import {Component, OnInit} from '@angular/core';
import {Exercise} from '../exercise';
import {HttpClient} from '@angular/common/http';

@Component({
  selector: 'app-exercise',
  templateUrl: './exercise.component.html',
  styleUrls: ['./exercise.component.css']
})
export class ExerciseComponent implements OnInit {

  exercises: Exercise[] = [];
  constructor(private http: HttpClient) {
    this.http.get<Exercise[]>('/api/exercises')
      .subscribe(exercise => {
        this.exercises = exercise; });
  }

  ngOnInit() {

  }

  open() {
  }
}
