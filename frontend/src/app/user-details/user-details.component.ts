import { Component, OnInit } from '@angular/core';
import {SecurityService} from '../security.service';
import {User} from '../user';
import {HttpClient} from '@angular/common/http';
import {WorkoutPlan} from '../workoutPlan';
import {WorkoutSession} from '../workoutsession';
import {Exercise} from '../exercise';
import {BehaviorSubject, Observable} from 'rxjs';
import * as CanvasJS from '../canvasjs.min';
import {Router} from '@angular/router';

@Component({
  selector: 'app-user-details',
  templateUrl: './user-details.component.html',
  styleUrls: ['./user-details.component.css']
})
export class UserDetailsComponent implements OnInit {
  isInvisible = false;
  counterValue = 0;
  sessionUser: User | null = null;
  private workoutPlan: WorkoutPlan[] = [];
  private sessionList: WorkoutSession[] = [];
  constructor(private router: Router, private httpService: HttpClient, private securityService: SecurityService) {
  }

  ngOnInit() {
    this.securityService.getSessionUser().subscribe(
      u => this.sessionUser = u
    );
    this.httpService.get<WorkoutPlan[]>('/api/workoutplan/' + this.sessionUser.username)
      .subscribe(workout => {
        this.workoutPlan = workout;
        for (const training of workout) {
          this.httpService.get<WorkoutSession[]>('/api/workoutsession/' + training.title)
            .subscribe(workoutsession => {
              this.sessionList = workoutsession;
              this.counterValue = this.workoutPlan[0].workoutWeek * 10 + (this.workoutPlan[0].workoutDay * 10) / workoutsession.length;
              this.drawchart();
            });
        }
      });
    this.isInvisible = false;
  }

  deletePlan(userName: string, title: string) {
    this.httpService.post<WorkoutPlan[]>('/api/deleteWorkoutplan/' + userName, title).subscribe(workout => {
      this.workoutPlan = workout;
      this.sessionList = [];
      for (const training of workout) {
        this.httpService.get<WorkoutSession[]>('/api/workoutsession/' + training.title)
          .subscribe(workoutsession => this.sessionList = workoutsession);
      }
    });
    this.isInvisible = true;
  }

  updateplan(week: number, day: number, woplanTitle: string): void {
    this.httpService.post<WorkoutPlan[]>('/api/workoutplan/' + this.sessionUser.username, woplanTitle)
      .subscribe(workout => {
        this.workoutPlan = workout;
        this.counter(workout[0].workoutWeek, workout[0].workoutDay);
        this.drawchart();
      });
  }

  resetplan(woplanTitle: string): void {
    this.httpService.post<WorkoutPlan[]>('/api/resetWorkoutplan/' + this.sessionUser.username, woplanTitle)
      .subscribe(workout => this.workoutPlan = workout);
    this.counter(0, 0);
    this.drawchart();
  }

  counter(week: number, day: number) {
    this.counterValue = week * 10 + day * 10 / this.sessionList.length;
    return this.counterValue;
  }
  drawchart() {
    const chart = new CanvasJS.Chart('chartContainer', {
      animationEnabled: false,
      exportEnabled: false,
      title: {
        text: 'Progress Bar'
      },
      axisY: {
        title: ' % ',
        interval: 10
      },
      data: [{
        type: 'bar',

        dataPoints: [
          {y: this.counterValue, label: 'Progress'},
          {y: 100, label: 'Goal'}

        ]
      }]
    });
    chart.render();
  }
}
