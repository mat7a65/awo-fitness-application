import {Component, OnInit} from '@angular/core';
import {Exercise} from '../exercise';
import {ActivatedRoute} from '@angular/router';
import {HttpClient} from '@angular/common/http';

@Component({
  selector: 'app-exercise-details',
  templateUrl: './exercise-details.component.html',
  styleUrls: ['./exercise-details.component.css']
})

export class ExerciseDetailsComponent implements OnInit {

  exercise: Exercise;

  constructor(private route: ActivatedRoute, private http: HttpClient) {
  }
  ngOnInit() {

    const exerciseTitle = this.route.snapshot.paramMap.get('exerciseTitle');
    this.http.get<Exercise>('/api/exercises/' + exerciseTitle)
      .subscribe(exercise => this.exercise = exercise)
    ;

  }


}
