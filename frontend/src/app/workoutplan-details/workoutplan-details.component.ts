import { Component, OnInit } from '@angular/core';
import {WorkoutPlan} from '../workoutPlan';
import {HttpClient} from '@angular/common/http';
import {ActivatedRoute} from '@angular/router';
import {WorkoutSession} from '../workoutsession';

@Component({
  selector: 'app-workoutplan-details',
  templateUrl: './workoutplan-details.component.html',
  styleUrls: ['./workoutplan-details.component.css']
})
export class WorkoutplanDetailsComponent implements OnInit {
  workoutTitle;
  workoutSession: WorkoutSession[] = [];
  constructor(private route: ActivatedRoute, private httpService: HttpClient) { }

  ngOnInit() {
    const workoutTitle = this.route.snapshot.paramMap.get('workoutTitle');
    this.workoutTitle = workoutTitle;
    this.httpService.get<WorkoutSession[]>('/api/workoutsession/' + this.workoutTitle)
      .subscribe(workout => this.workoutSession = workout);
  }
}
