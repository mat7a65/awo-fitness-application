import { Component, OnInit } from '@angular/core';
import {UserExercise} from '../userexercise';
import {HttpClient} from '@angular/common/http';
import {ActivatedRoute, Router} from '@angular/router';
import {UserDTO} from '../userDTO';

@Component({
  selector: 'app-user-exercise',
  templateUrl: './user-exercise.component.html',
  styleUrls: ['./user-exercise.component.css']
})
export class UserExerciseComponent implements OnInit {
  userName: string;
  exerciseTitel: string;
  userExercise: UserExercise = {dauer: 0, impression: '', puls: 0, repetitions: 0, sets: 0, weight: 0};
  constructor(private http: HttpClient, private route: ActivatedRoute, private router: Router) { }

  ngOnInit() {
    this.userName = this.route.snapshot.paramMap.get('userName');
    this.exerciseTitel = this.route.snapshot.paramMap.get('exerciseTitel');
    this.http.get<UserExercise>('/api/userExercise/' + this.userName + '/' + this.exerciseTitel)
      .subscribe(userexercise => this.userExercise = userexercise);
  }
  submit() {
    this.http.post<UserExercise>('/api/userExercise/' + this.userName + '/' + this.exerciseTitel, this.userExercise)
      .subscribe(() => {});
    this.router.navigate(['/overview']);
  }
}
