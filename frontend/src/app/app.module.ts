import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { SessionUserComponent } from './session-user/session-user.component';
import {HttpClientModule} from '@angular/common/http';
import { LoginComponent } from './login/login.component';
import {FormsModule} from '@angular/forms';
import { ExerciseComponent } from './exercise/exercise.component';
import { RegisterComponent } from './register/register.component';
import { ExerciseDetailsComponent } from './exercise-details/exercise-details.component';
import { UserDetailsComponent } from './user-details/user-details.component';
import { WorkoutplanComponent } from './workoutplan/workoutplan.component';
import { WorkoutplanDetailsComponent } from './workoutplan-details/workoutplan-details.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { ExerciseCategoryComponent } from './exercise-category/exercise-category.component';
import { DisclaimerComponent } from './disclaimer/disclaimer.component';
import { UserExerciseComponent } from './user-exercise/user-exercise.component';
import { HomeComponent } from './home/home.component';

@NgModule({
  declarations: [
    AppComponent,
    SessionUserComponent,
    LoginComponent,
    ExerciseComponent,
    LoginComponent,
    RegisterComponent,
    ExerciseDetailsComponent,
    ExerciseDetailsComponent,
    UserDetailsComponent,
    WorkoutplanComponent,
    WorkoutplanDetailsComponent,
    ExerciseCategoryComponent,
    DisclaimerComponent,
    UserExerciseComponent,
    HomeComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    NgbModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
