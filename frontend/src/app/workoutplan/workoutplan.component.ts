import {Component, OnInit} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {WorkoutPlan} from '../workoutPlan';
import {User} from '../user';
import {SecurityService} from '../security.service';
import {Router} from '@angular/router';
import {WorkoutSession} from '../workoutsession';


@Component({
  selector: 'app-workoutplan',
  templateUrl: './workoutplan.component.html',
  styleUrls: ['./workoutplan.component.css']
})
export class WorkoutplanComponent implements OnInit {
  sessionUser: User | null = null;
  private workoutPlan: WorkoutPlan[] = [];
  private sessionList: WorkoutSession[] = [];

  title: string;
  userName: string;

  constructor(private httpService: HttpClient, private securityService: SecurityService, private router: Router) {
  }

  choosePlan(userName: string, title: string) {
    this.userName = userName;
    this.title = title;
    this.httpService.post<WorkoutPlan[]>('/api/setWorkoutplan/' + this.userName, this.title)
      .subscribe(workout => this.workoutPlan = workout);
    this.router.navigate(['/overview']);
  }


  deletePlan(userName: string, title: string) {
    this.httpService.post<WorkoutPlan[]>('/api/deleteWorkoutplan/' + userName, title).subscribe(() => {
      this.httpService.get<WorkoutPlan[]>('/api/workoutplan/')
        .subscribe(workout => this.workoutPlan = workout);
    });
  }


  ngOnInit() {
    this.httpService.get<WorkoutPlan[]>('/api/workoutplan/')
      .subscribe(workout => this.workoutPlan = workout);
    this.securityService.getSessionUser().subscribe(
      u => this.sessionUser = u
    );
  }
}
