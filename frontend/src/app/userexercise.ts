export interface UserExercise {
  sets: number;
  repetitions: number;
  weight: number;
  dauer: number;
  puls: number;
  impression: string;
}
